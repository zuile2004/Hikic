# 欢迎使用Hikic！
### Hikic是被用来简化复杂网页脚本的轻量级无依赖JavaScript库。
### Hikic可以实现许多JQuery库可以实现的功能，同时抛弃了许多JQuery库不常使用的方法，Hikic库比JQuery库更加轻量级，同时也更加实用，比如说cookie这方面。
### 另外Hikic，还有着强大的AJAX、Cookie、事件和操作DOM的能力，你几乎可以用它去做任何JS能做的事情，而我们的目标，就是“ 做更强大的JS，用更简洁的代码 ”。
### 你可以看到，Hikic1.0 Community的代码总大小不到9kb，和JQuery相比，极其轻量级。
### 同时Hikic已经可以兼容（IE6.0更低、IE6.0+、google chrome、firefox、safari、opera）等等......
#### 特此说明：Hikic动画并不包含在Hikic的JS文件中，如有需要，请下载HikicAnimation.css（预计2018-05-01推出）
<hr>
 <p><b>以下是Hikic的方法和功能说明：（目前为1.0版本）</b></p>
 <p> <b>Hikic元素选择器：_("")</b></p>
<code>_("#test").方法</code>
<p><b>Hikic目前具有：</b></p> 

- val方法-获取或设置元素的value值

- html方法-获取或设置元素包含的html

- text方法-获取或设置元素包含的文本

- attr方法-获取或设置元素的属性值

- addClass方法-给元素添加一条class属性

- hasClass方法-获取元素所具有的class属性

- removeAttr方法-删除元素的一个属性

- removeClass方法-删除元素的一个class属性里的class值

- removeHTML方法-删除所选元素

- empty方法-删除所选元素的所有子元素

- css方法-获取或设置所选元素的css属性

- height方法-获取或设置所选元素的height属性

- width方法-获取或设置所选元素的width属性

- innerWidth方法-获取元素内部宽度

- innerHeight方法-获取元素内部高度

- parent方法-获取元素的父节点

- children方法-获取元素的所有子节点

- load方法-调用AJAX，并将结果存储在元素中

- cookie方法-设置和替换cookie，（path不填写则默认为/）,如果cookie名称一样则替换

- getCookieVal方法-获取cookie的值

- delCookie方法-删除一个cookie

- click方法-设置元素的点击事件

- mousemove方法-设置元素的鼠标移动事件

- mousedown方法-设置元素的鼠标按下事件

- mouseup方法-设置元素的鼠标抬起事件

- mouseout方法-设置元素的鼠标移出事件

- mouseover方法-设置元素的鼠标移入事件

- ready（相当于onload）方法-设置元素的加载完成事件

- focus方法-设置元素获取到焦点的事件

- blur方法-设置元素失去焦点的事件

- lazyLoad方法-设置元素懒加载

- setLazyLoadLRT方法-设置元素懒加载监听器重复时间

- setLazyLoadRange方法-设置元素懒加载识别范围